<?php

namespace Drupal\rut_field\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validation constraint for unique rut.
 *
 * @Constraint(
 *   id = "UniqueRutField",
 *   label = @Translation("Unique Rut Field.", context = "Validation"),
 * )
 */
class UniqueRutFieldConstraint extends Constraint {

  /**
   * The default violation message.
   *
   * @var string
   */
  public $message = 'A @entity_type with @field_name %value already exists.';

}
