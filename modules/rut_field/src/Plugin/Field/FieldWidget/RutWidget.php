<?php

namespace Drupal\rut_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\rut\Rut;

/**
 * Plugin implementation of the 'rut_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "rut_field_widget",
 *   module = "rut_field",
 *   label = @Translation("Rut Element"),
 *   field_types = {
 *     "rut_field_rut"
 *   }
 * )
 */
class RutWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'validate_js' => TRUE,
      'message_js' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Bypass RUT validation: @bypass', ['@bypass' => ($this->getFieldSetting('bypass_validation') ? $this->t('Yes') : $this->t('No'))]);
    $validate_js = $this->getSetting('validate_js');
    $summary[] = $this->t('Use Javascript: @validate_js', ['@validate_js' => ($validate_js ? $this->t('Yes') : $this->t('No'))]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();
    $element['validate_js'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Javascript'),
      '#default_value' => $this->getSetting('validate_js'),
      '#description' => $this->t('The value will be formatted as each digit is entered.'),
    ];

    $element['message_js'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Define the message to display if the value is not a valid rut.'),
      '#default_value' => $this->getSetting('message_js'),
      '#access' => !$this->getFieldSetting('bypass_validation'),
      '#attributes' => [
        'placeholder' => $this->t('Invalid Rut'),
      ],
      '#states' => [
        'visible' => [
          'input[name="fields[' . $field_name . '][settings_edit_form][settings][validate_js]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    if (isset($items[$delta]->value)) {
      $default_value = $items[$delta]->value;
    }
    else {
      $rut = isset($items[$delta]->rut) ? $items[$delta]->rut : '';
      $dv = isset($items[$delta]->dv) ? $items[$delta]->dv : '';
      $default_value = Rut::formatterRut($rut, $dv);
    }

    $field_definition = $items->getFieldDefinition();
    $rut_element = [
      '#type' => 'rut_field',
      '#default_value' => $default_value,
      '#bypass_validation' => $field_definition->getSetting('bypass_validation'),
    ];

    if ($this->getSetting('validate_js')) {
      $rut_element['#validate_js'] = TRUE;
      $rut_element['#message_js'] = $this->getSetting('message_js');
    }

    $element += $rut_element;

    return ['value' => $element];
  }

}
