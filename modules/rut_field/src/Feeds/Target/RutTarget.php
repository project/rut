<?php

namespace Drupal\rut_field\Feeds\Target;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Exception\TargetValidationException;
use Drupal\feeds\FieldTargetDefinition;
use Drupal\feeds\Plugin\Type\Target\ConfigurableTargetInterface;
use Drupal\feeds\Plugin\Type\Target\FieldTargetBase;
use Drupal\rut\Rut;

/**
 * Defines the rut field mapper.
 *
 * @FeedsTarget(
 *   id = "rut_field",
 *   field_types = {
 *     "rut_field_rut"
 *   }
 * )
 */
class RutTarget extends FieldTargetBase implements ConfigurableTargetInterface {

  /**
   * {@inheritdoc}
   */
  protected static function prepareTarget(FieldDefinitionInterface $field_definition) {
    return FieldTargetDefinition::createFromFieldDefinition($field_definition)
      ->addProperty('value')
      ->addProperty('rut')
      ->addProperty('dv')
      ->markPropertyUnique('value')
      ->markPropertyUnique('rut');
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareValue($delta, array &$values) {
    if (isset($values['value'])) {
      $values['value'] = trim($values['value']);
      $values['rut'] = '';
      $values['dv'] = '';
    }
    elseif (!empty($values['rut']) && !empty($values['dv'])) {
      $values['value'] = Rut::formatterRut($values['rut'], $values['dv']);
    }
    else {
      $values['value'] = '';
    }

    if ($values['value'] && !Rut::validateRut($values['value'])) {
      if ($this->configuration['validation_exception']) {
        throw new TargetValidationException($this->t('The rut is invalid'));
      }
      $values['value'] = '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['validation_exception' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['validation_exception'] = [
      '#type' => 'radios',
      '#title' => $this->t('If the RUT is invalid'),
      '#default_value' => $this->configuration['validation_exception'],
      '#options' => [
        TRUE => $this->t('The import of the content will be canceled.'),
        FALSE => $this->t('It will be replaced to an empty value.'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return $this->configuration['validation_exception'] ?
      $this->t('The content will be not imported if the rut is invalid.') :
      $this->t('The rut will be replaced with an empty value if it is invalid.');
  }

}
