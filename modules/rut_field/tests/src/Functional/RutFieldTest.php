<?php

namespace Drupal\Tests\rut_field\Functional;

use Drupal\rut\Rut;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the creation of rut_field fields.
 *
 * @group rut_field
 */
class RutFieldTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'field',
    'node',
    'rut_field',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to create articles.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * Set a content type to use during the testing.
   */
  protected function setUp() :void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'article']);
    $this->webUser = $this->drupalCreateUser([
      'create article content',
      'edit own article content',
    ]);
    $this->drupalLogin($this->webUser);
  }

  /**
   * Helper function for testRutField().
   */
  public function testRutField() {
    // Add the rut_field field to the article content type.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_rut_field',
      'entity_type' => 'node',
      'type' => 'rut_field_rut',
    ]);
    $field_storage->save();
    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'article',
    ])->save();

    $entity_form_display = \Drupal::entityTypeManager()
      ->getStorage('entity_form_display')
      ->load('node.article.default');
    $entity_form_display->setComponent('field_rut_field')
      ->save();

    $entity_view_display = \Drupal::entityTypeManager()
      ->getStorage('entity_view_display')
      ->load('node.article.default');

    $entity_view_display->setComponent('field_rut_field')
      ->save();

    // Display creation form.
    $this->drupalGet('node/add/article');
    $this->assertSession()->fieldExists("field_rut_field[0][value]");

    // Test basic entry of rut_field field.
    $randomRut = Rut::generateRut();
    $edit = [
      'title[0][value]' => $this->randomMachineName(),
      'field_rut_field[0][value]' => $randomRut,
    ];

    $this->submitForm($edit, 'Save');
    $this->assertSession()->responseContains($randomRut);

    // Add rut without format. Then check if show the same rut with format.
    list($rut, $dv) = Rut::generateRut(FALSE);
    $edit = [
      'title[0][value]' => $this->randomMachineName(),
      'field_rut_field[0][value]' => $rut . $dv,
    ];
    $rutFormatted = Rut::formatterRut($rut, $dv);
    $this->drupalGet('node/add/article');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->responseContains($rutFormatted);

    // Edit the node.
    $this->drupalGet('node/2/edit');
    $this->assertSession()->fieldValueEquals("field_rut_field[0][value]", $rutFormatted);
    $randomRut = Rut::generateRut();
    $edit = [
      'field_rut_field[0][value]' => $randomRut,
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->responseContains($randomRut);
  }

}
