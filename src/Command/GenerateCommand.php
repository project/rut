<?php

namespace Drupal\rut\Command;

use Drupal\Console\Annotations\DrupalCommand;
use Drupal\Console\Core\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\rut\Rut;

/**
 * Class GenerateCommand.
 *
 * @DrupalCommand (
 *     extension="rut",
 *     extensionType="module"
 * )
 */
class GenerateCommand extends Command {

  /**
   * {@inheritdoc}
   */
  protected function configure() {

    $this
      ->setName('rut:generate')
      ->setDescription($this->trans('commands.rut.generate.description'))
      ->setHelp($this->trans('commands.rut.generate.help'))
      ->addArgument('quantity', InputArgument::OPTIONAL, $this->trans('commands.rut.generate.arguments.quantity'), 1)
      ->addOption('min', NULL, InputOption::VALUE_OPTIONAL, $this->trans('commands.rut.generate.options.min'), 1)
      ->addOption('max', NULL, InputOption::VALUE_OPTIONAL, $this->trans('commands.rut.generate.options.max'), 20000000);
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {

    $io = new DrupalStyle($input, $output);

    $quantity = (int) $input->getArgument('quantity');

    if ($quantity < 1) {
      $io->error($this->trans('commands.rut.generate.errors.invalid-quantity'));

      return;
    }

    if ($quantity > 20) {
      $io->info($this->trans('commands.rut.generate.messages.quantity-limit'));
      $quantity = 20;
    }

    $tableHeader = [
      'Rut',
      'DV',
      ' ',
      $this->trans('commands.rut.generate.messages.formatted'),
    ];
    $tableRows = [];

    $min = (int) $input->getOption('min');
    $max = (int) $input->getOption('max');

    $io->success(
        sprintf($this->trans('commands.rut.generate.messages.success'), $quantity)
    );

    for ($i = 0; $i < $quantity; $i++) {
      list($rut, $dv) = Rut::generateRut(FALSE, $min, $max);
      $tableRows[] = [
        $rut,
        $dv,
        ' ',
        Rut::formatterRut($rut, $dv),
      ];
    }

    $io->table($tableHeader, $tableRows, 'compact');
  }

}
