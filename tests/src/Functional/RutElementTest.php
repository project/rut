<?php

namespace Drupal\Tests\rut\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the rut_field element.
 *
 * @group rut
 */
class RutElementTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['rut_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that #type 'rut_field' fields are properly validated.
   */
  public function testFormRut() {
    $edit = [];
    $edit['rut'] = '1-2';
    $edit['rut_required'] = ' ';
    $this->drupalGet('rut-test');
    $this->submitForm($edit, 'Submit');
    $this->assertSession()->responseContains(t('The Rut/Run @rut is invalid.', ['@rut' => '1-2']));
    $this->assertSession()->responseContains(t('@name field is required.', ['@name' => 'Rut']));

    $edit = [];
    $edit['rut'] = '';
    $edit['rut_required'] = '1-9';
    $this->submitForm($edit, 'Submit');
    $values = Json::decode($this->getSession()->getPage()->getContent());
    $this->assertSame($values['rut'], '');
    $this->assertEquals('1-9', $values['rut_required']);

    $edit = [];
    $edit['rut'] = '11.111.111-1';
    $edit['rut_required'] = '1-9';
    $this->drupalGet('rut-test');
    $this->submitForm($edit, 'Submit');
    $values = Json::decode($this->getSession()->getPage()->getContent());
    $this->assertEquals('11.111.111-1', $values['rut']);
    $this->assertEquals('1-9', $values['rut_required']);
  }

}
