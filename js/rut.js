/**
 * @file
 * Setting the elements to use jquery plugin rut.
 */

(function ($) {
  "use strict";
  Drupal.behaviors.rutField = {
    attach: function (context) {
      once('rut-processed', '.rut-field-input.rut-validate-js', context).forEach(function(input) {
        const $this = $(input);
        const $message = $this.parent('.form-item').children('.error-message-js');

        const rutSettings = {
          format_on: 'keyup'
        };
        if (!$this.hasClass('rut-bypass-validation')) {
          rutSettings.on_error = function () {
            $this.addClass('error');
            $message.removeClass('invisible');
          };
          rutSettings.on_success = function () {
            $this.removeClass('error');
            $message.addClass('invisible');
          };
        }

        $this.Rut(rutSettings);
      });
    }
  };
})(jQuery);
